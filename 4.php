<?php

// Зміні
$num = 32;
$num_2 = 23.45;
$bool = false;
$string = "Hello World!";
echo "IS: " . $string;

define ("PI", 3.14);
echo PI;

// АРИФМЕТИЧНІ ОПЕРАЦІ 
$x = 20;
$y = 6;
$res;
$res = $x + $y;
echo $res."<br>";
$res = $x - $y;
echo $res."<br>";
$res = $x * $y;
echo $res."<br>";
$res = $x / $y;
echo $res."<br>";
$res = $x % $y;
echo $res."<br>";

// умовні опратори
$x = 201;
$b = true;
if ($x <= 200 || $b) {
	echo "$x is less than 200";
	if ($x != 150)
		echo "<br>150";
}
else if ($x == 425)
	echo "$x is equal 425";
else if ($x >= 425)
	echo "$x is bigger than 425";
else
	echo "$x is unknown";
	
$str = $x == 201 ? "yes" : "no";

// ОПЕРАТОР Switch
$x = 100;
switch ($x) {
	case 2: 
		echo "x is 2";
		break;
	case 78: 
		echo "x is 78";
		break;
	case "Hi": 
		echo "x is Hi";
		break;
	case 1: 
		echo "x is 1";
		break;
	default:
		echo "x is unknown";
}

// Цикли
$i = 0;
do {
	echo "Element i is: ".$i."<br>";
} while ($i > 10);

echo "<hr>";

$i = 0;
while ($i < 10) {
	echo "Element i is: ".$i."<br>";	
	$i++;
}

echo "<hr>";	

for ($i = 100; $i >= 10; $i -= 10) {
	if ($i == 80)
		break;
	echo "Element i is: ".$i."<br>";	
}

// масив
$arr = array(12, 45, 67, 45.23, "Hi", true);
for ($i = 0; $i < count ($arr); $i++)
	echo "Element $i is: ".$arr[$i]."<br>";
	
$array = array("age" => 18, "name" => "George");
foreach ($array as $key => $value)
	echo "Element with key ".$key." is: ".$value."<br>";
	
// Двовимірні масиви
$arr = array(array(12, 34.45, "Hi"), array(true, 56), array(56, 7, true, "Hello"));
for ($i = 0; $i < count ($arr); $i++) {
	for ($j = 0; $j < count ($arr[$i]); $j++) {
		echo $arr[$i][$j]."  ";
	}
	echo "<br>";
}

// Функції
function printWords ($word) {
	echo $word;
}

function summa ($x, $y) {
	$res = $x + $y;
	return $res;
}

$str = "Hello<br>";
printWords ($str);
$x = summa (6, 5);
printWords ($x);

// $_COOKIE
$num = isset($_COOKIE['num']) ? $_COOKIE['num'] : 0;
$num++;
setcookie("num", $num, time () + 6);
echo "Update page $num times!";
 ?>